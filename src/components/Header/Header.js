import { MenuItem, TextField, ThemeProvider } from '@material-ui/core';
import { createTheme } from '@material-ui/core/styles';
import categories from '../../data/category';
import React from 'react';
import './Header.css';
const Header = ({ category, setCategory, word, setWord, lightMode }) => {
  const darktheme = createTheme({
    palette: {
      primary: {
        main: lightMode ? '#000' : '#fff',
      },
      type: lightMode ? 'light' : 'dark',
    },
  });

  const handleChange = (language) => {
    setCategory(language);
    setWord('');
  };

  return (
    <div className='header'>
      <span className='title'>Dictionary App</span>
      <div className='inputs'>
        <ThemeProvider theme={darktheme}>
          <TextField
            className='search'
            label='Search..'
            value={word}
            onChange={(e) => setWord(e.target.value)}
            variant='outlined'
          />
          <TextField
            className='select'
            select
            label='Language'
            value={category}
            onChange={(e) => handleChange(e.target.value)}
            variant='outlined'
          >
            {categories.map((option) => (
              <MenuItem key={option.label} value={option.label}>
                {option.value}
              </MenuItem>
            ))}
          </TextField>
        </ThemeProvider>
      </div>
    </div>
  );
};

export default Header;
